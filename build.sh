#!/bin/bash
set -eu
DESIRED_COVERAGE=80
MAVEN_CLI_OPTS="-s .m2/settings.xml --batch-mode"
export MAVEN_OPTS="-Dmaven.repo.local=.m2/repository"
mvn $MAVEN_CLI_OPTS package
echo "Code coverage results in file:///home/roland/src/gitlab.com/nginscale/code-cov-example-java/target/site/jacoco/index.html"
COVERAGE=`awk -F"," '{ instructions += $4 + $5; covered += $5 } END { print 100*covered/instructions }' target/site/jacoco/jacoco.csv`
echo "coverage: ${COVERAGE}% of statements"
if (( COVERAGE < DESIRED_COVERAGE )); then
    echo "WARNING: This is less than the desired coverage of ${DESIRED_COVERAGE}%"
fi
