#!/bin/sh
set -eu
. ./env.default
rm -rf target
# build class files
docker run -v "${PWD}:/host" -w /host -e MAVEN_CONFIG=/host/.m2 --user $(id -u):$(id -g) maven:3.6.1-jdk-11-slim ./build.sh
# build docker image
docker build -f Dockerfile -t "$START_APP_IMAGE" .
